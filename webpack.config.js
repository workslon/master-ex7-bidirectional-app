module.exports = {
  entry: [
    // styles
    'bootstrap-css-only/css/bootstrap.css',
    './src/css/app.css',

    // scripts
    './src/js/app.js'
  ],

  output: {
    path: './build',
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      {test: /\.js$/, include: /src/,  loaders: ['babel']},
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url?limit=100000'}
    ]
  }
};