var React = require('react');
var ReactDOM = require('react-dom');
var StatusConstants = require('../constants/StatusConstants');
var Link = require('react-router').Link;
var Publisher = require('./Publisher.react');

module.exports = React.createClass({
  displayName: 'PublisherList',

  render: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    return (
      <div>
        <Link className="create-publisher btn btn-success bt-sm" to="/publishers/create">+ Add Publisher</Link>
        {status === StatusConstants.PENDING && <p className="bg-info">Deleting...</p>}
        {this.props.publishers.length ?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Books</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.publishers.map((function (publisher, i) {
                return (
                  <Publisher key={i} nr={i + 1} publisher={publisher} />
                );
              }).bind(this))}
            </tbody>
          </table>
          : <div>Loading publishers...</div>}
      </div>
    );
  }
});