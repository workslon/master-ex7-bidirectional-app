var mODELcLASS = require('mODELcLASS');

module.exports = new mODELcLASS({
  name: 'Publisher',
  properties: {
    name: {
      label: 'Name',
      range: 'NonEmptyString',
      min: 2,
      max: 50
    },

    email: {
      label: 'Email',
      range: 'NonEmptyString',
      isStandardId: true,
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      patternMessage: 'You entered invalid email!'
    },

    publishedBooks: {
      label: 'Books',
      className: 'Book',
      range: 'MultiValueRef',
      optional: true
    }
  }
});